import { Component } from '@angular/core';
import { APIService } from './services/api.service';
import {
  UserInfo,
  EmployeeInfo,
  DistributerInfo,
  ProductInfo,
  AccessBlocksInfo,
} from './Models/userInfo';
import { StorageServiceService } from './services/storage-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Crunchersoft Technologies';

  constructor(
    private apiService: APIService,
    private storageService: StorageServiceService
  ) {
    console.log('in constructor');

  }
}
