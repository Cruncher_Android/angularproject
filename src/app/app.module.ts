import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material/material.module';
import { AppComponent } from './app.component';
import { RegistrationComponentComponent } from './components/registration-component/registration-component.component';
import { RegistrationFormComponent } from './components/registration-component/registration-form/registration-form.component';
import { InboxComponentComponent } from './components/inbox-component/inbox-component.component';
import { DemoListComponentComponent } from './components/demo-list-component/demo-list-component.component';
import { HistoryComponentComponent } from './components/history-component/history-component.component';
import { UserDetailsComponentComponent } from './components/user-details-component/user-details-component.component';
import { AdminDashboardComponentComponent } from './components/admin-dashboard-component/admin-dashboard-component.component';
import { EmployeeListComponentComponent } from './components/admin-dashboard-component/employee-list-component/employee-list-component.component';
import { DistributerListComponentComponent } from './components/admin-dashboard-component/distributer-list-component/distributer-list-component.component';
import { ProductListComponentComponent } from './components/admin-dashboard-component/product-list-component/product-list-component.component';
import { AccessBlocksComponentComponent } from './components/admin-dashboard-component/access-blocks-component/access-blocks-component.component';
import { FooterComponentComponent } from './components/footer-component/footer-component.component';
import { EmployeeDashboardComponentComponent } from './components/employee-dashboard-component/employee-dashboard-component.component';
import { RegisterationComponentComponent } from './components/employee-dashboard-component/registeration-component/registeration-component.component';
import { LoginComponentComponent } from './components/login-component/login-component.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { DistributerDashboardComponentComponent } from './components/distributer-dashboard-component/distributer-dashboard-component.component';

@NgModule({
  entryComponents: [UserDetailsComponentComponent],

  declarations: [
    AppComponent,
    RegistrationComponentComponent,
    RegistrationFormComponent,
    InboxComponentComponent,
    DemoListComponentComponent,
    HistoryComponentComponent,
    UserDetailsComponentComponent,
    AdminDashboardComponentComponent,
    EmployeeListComponentComponent,
    DistributerListComponentComponent,
    ProductListComponentComponent,
    AccessBlocksComponentComponent,
    FooterComponentComponent,
    EmployeeDashboardComponentComponent,
    RegisterationComponentComponent,
    LoginComponentComponent,
    DistributerDashboardComponentComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    LayoutModule,
    BsDatepickerModule.forRoot(),
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
