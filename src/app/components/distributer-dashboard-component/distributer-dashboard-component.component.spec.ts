import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributerDashboardComponentComponent } from './distributer-dashboard-component.component';

describe('DistributerDashboardComponentComponent', () => {
  let component: DistributerDashboardComponentComponent;
  let fixture: ComponentFixture<DistributerDashboardComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistributerDashboardComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributerDashboardComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
