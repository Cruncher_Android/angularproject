import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState,
} from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { APIService } from 'src/app/services/api.service';
import { demoInfo, UserInfo } from 'src/app/Models/userInfo';
import { StorageServiceService } from 'src/app/services/storage-service.service';

@Component({
  selector: 'app-distributer-dashboard-component',
  templateUrl: './distributer-dashboard-component.component.html',
  styleUrls: ['./distributer-dashboard-component.component.scss'],
})
export class DistributerDashboardComponentComponent implements OnInit {
  isHandset: Observable<BreakpointState>;
  distributerName: string = '';
  distributerId: string = '';
  selectedMenuItem: string = 'register';
  isDistributerLogin: boolean = true;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private breakpointObserver: BreakpointObserver,
    private apiService: APIService,
    public storageServiceService: StorageServiceService
  ) {}

  ngOnInit(): void {
    this.isHandset = this.breakpointObserver.observe(Breakpoints.Handset);
    this.distributerName = this.activatedRoute.snapshot.params[
      'distributerName'
    ];
    this.distributerId = this.activatedRoute.snapshot.params['distributerId'];
    if (this.distributerId.length == 1)
      this.distributerId = `0${this.distributerId}`;
    this.getDemoList(this.distributerId);
    this.getAllUsers(this.distributerId);
  }

  getAllUsers(distributerId) {
    this.apiService
      .getAllUsersDistributerWise(distributerId)
      .subscribe((data: UserInfo[]) => {
        console.log("in all users", data)
        this.storageServiceService.allUsers = data;
      });
  }

  getDemoList(distributerId) {
    this.apiService
      .getDistributerWiseDemoDetails(distributerId)
      .subscribe((data: demoInfo[]) => {
        console.log(data);
        this.storageServiceService.demoUsers = data;
      });
  }

  onManuClick(event) {
    this.selectedMenuItem = event.target.value;
  }

  onLogoutClick() {
    if (confirm('Do you want to sign out...?')) {
      this.storageServiceService.loginDetails = null;
      this.router.navigate(['/login']);
      this.storageServiceService.allUsers = [];
      this.storageServiceService.demoUsers = [];
    }
  }
}
