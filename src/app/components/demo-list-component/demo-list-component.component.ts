import { Component, OnInit, Input } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { demoInfo } from 'src/app/Models/userInfo';
import { APIService } from './../../services/api.service';
import { StorageServiceService } from './../../services/storage-service.service';

@Component({
  selector: 'app-demo-list-component',
  templateUrl: './demo-list-component.component.html',
  styleUrls: ['./demo-list-component.component.scss'],
})

export class DemoListComponentComponent implements OnInit {
  @Input() isDistributerLogin: boolean;
  @Input() distributerId: string = '';
  userInfo: demoInfo[] = [];
  filteredUsers: demoInfo[] = [];
  activeMonth: string = '';
  activeDist: string = '';
  distributerName: string = '';
  monthName: string = '';
  dist: string = '';
  showAdvancedSearchPanel: boolean = false;
  fromDate: string = '';
  toDate: string = '';
  fromDateString: string = '';
  toDateString: string = '';
  datepickerConfig: Partial<BsDatepickerConfig>;

  constructor(
    private apiService: APIService,
    public storageService: StorageServiceService
  ) {
    this.datepickerConfig = {
      containerClass: 'theme-dark-blue',
      showWeekNumbers: false,
      dateInputFormat: 'DD/MM/YYYY',
      isAnimated: true,
      useUtc: false,
    };
  }

  ngOnInit(): void {
    this.userInfo = [...this.storageService.demoUsers];
    this.filteredUsers = [...this.storageService.demoUsers];
  }

  onClearAllFilters() {
    this.activeMonth = '';
    this.activeDist = '';
    this.dist = '';
    this.distributerName = '';
    this.monthName = '';
    this.userInfo = [...this.storageService.demoUsers];
    this.filteredUsers = [...this.storageService.demoUsers];
  }

  filterByDistributer(id, name) {
    this.dist = id.toString();
    if (this.dist.length == 1) this.activeDist = '0' + this.dist;
    else this.activeDist = this.dist;
    this.distributerName = name;
  }

  filterByMonth(id, name) {
    this.activeMonth = id;
    this.monthName = name;
  }

  onClearMonthFilter() {
    this.activeMonth = '';
    this.monthName = '';
  }

  onClearDistFilter() {
    this.activeDist = '';
    this.dist = '';
    this.distributerName = '';
  }

  applyFilter() {
    console.log(this.activeDist);
    // console.log(typeof(this.userInfo[0].date.split('-')[1]));
    this.userInfo = [...this.storageService.demoUsers];
    if (this.activeMonth !== '') {
      this.userInfo = this.userInfo.filter(
        (user) =>
          user.date !== null &&
          user.date.split('-')[1].toString() === this.activeMonth
      );
    }
    if (this.activeDist !== '') {
      this.userInfo = this.userInfo.filter(
        (user) => user.distributerId === this.activeDist
      );
    }
    this.filteredUsers = [...this.userInfo];
  }

  handleSearch(event) {
    let query: string = event.target.value;
    let userInfo: demoInfo[] = [];
    userInfo = this.filteredUsers.filter((user) =>
      user.firstName.toLowerCase().startsWith(query.toLowerCase())
    );
    this.userInfo = [...userInfo];
  }

  onAdvancedSearch() {
    this.showAdvancedSearchPanel = true;
  }

  onClose() {
    this.showAdvancedSearchPanel = false;
    this.fromDate = '';
    this.toDate = '';
    this.userInfo = this.storageService.demoUsers;
  }

  onSearchButton() {
    this.toDateString = this.getFormatedDate(this.toDate.toString().split(' '));
    this.fromDateString = this.getFormatedDate(
      this.fromDate.toString().split(' ')
    );

    // console.log(
    //   'toDateString',
    //   this.toDateString,
    //   'fromDateString',
    //   this.fromDateString
    // );

    this.apiService
      .getBetweenTimeStampDemo(
        {
          fromDate: this.fromDateString,
          toDate: this.toDateString,
        },
        this.distributerId
      )
      .subscribe((data: demoInfo[]) => {
        this.userInfo = data;
      });
  }

  getFormatedDate(dateArray) {
    let date = dateArray[2];
    if (Number.parseInt(date) < 10) date = date.substr(1);
    let month = dateArray[1];
    if (month == 'Jan') month = '1';
    if (month == 'Feb') month = '2';
    if (month == 'Mar') month = '3';
    if (month == 'Apr') month = '4';
    if (month == 'May') month = '5';
    if (month == 'Jun') month = '6';
    if (month == 'Jul') month = '7';
    if (month == 'Aug') month = '8';
    if (month == 'Sep') month = '9';
    if (month == 'Oct') month = '10';
    if (month == 'Nov') month = '11';
    if (month == 'Dec') month = '12';
    let year = dateArray[3];
    return `${year}-${month}-${date}`;
  }

}
