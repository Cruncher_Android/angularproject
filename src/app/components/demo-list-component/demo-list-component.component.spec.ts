import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoListComponentComponent } from './demo-list-component.component';

describe('DemoListComponentComponent', () => {
  let component: DemoListComponentComponent;
  let fixture: ComponentFixture<DemoListComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoListComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoListComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
