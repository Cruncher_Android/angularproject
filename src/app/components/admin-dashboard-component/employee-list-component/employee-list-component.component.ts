import { Component, OnInit } from '@angular/core';
import { APIService } from './../../../services/api.service';
import { EmployeeInfo } from 'src/app/Models/userInfo';
import { StorageServiceService } from './../../../services/storage-service.service';

@Component({
  selector: 'app-employee-list-component',
  templateUrl: './employee-list-component.component.html',
  styleUrls: ['./employee-list-component.component.scss'],
})
export class EmployeeListComponentComponent implements OnInit {
  employee: EmployeeInfo;
  emp: EmployeeInfo;
  accessBlocks: string[] = [];

  constructor(
    private apiService: APIService,
    public storageService: StorageServiceService
  ) {}

  ngOnInit(): void {
    this.employee = {
      id: 0,
      fullName: '',
      email: '',
      accessBlocks: '',
      password: '',
      activate: 'true',
      role: 'employee',
    };
  }

  onEditEmployee(employee) {
    this.employee = { ...employee };
    this.emp = employee;
    this.accessBlocks = this.employee.accessBlocks.split(',');
  }

  onSaveEmployee() {
    let employeeExists: boolean = false;
    let employeeArray = [...this.storageService.employeeInfo];
    this.employee.accessBlocks = this.accessBlocks.join(',');
    let index = this.storageService.employeeInfo.indexOf(this.emp);
    if (index < 0) {
      employeeArray.map((employee) => {
        if (employee.email == this.employee.email) {
          employeeExists = true;
          alert('Employee already exists');
          this.employee = {
            id: 0,
            fullName: '',
            email: '',
            accessBlocks: '',
            password: '',
            activate: 'true',
            role: 'employee',
          };
          this.emp = {
            id: 0,
            fullName: '',
            email: '',
            accessBlocks: '',
            password: '',
            activate: 'true',
            role: 'employee',
          };
          this.accessBlocks = [];
        }
      });
      if (!employeeExists) {
        if (this.storageService.employeeInfo.length > 0) {
          let id =
            this.storageService.employeeInfo[
              this.storageService.employeeInfo.length - 1
            ].id + 1;
          this.employee.id = id;
        } else {
          this.employee.id = 1;
        }
        this.storageService.employeeInfo.push(this.employee);
        this.save(employeeArray);
      }
    } else {
      this.storageService.employeeInfo[index] = this.employee;
      this.save(employeeArray);
    }
  }

  save(employeeArray) {
    this.apiService.saveEmployeeDetails(this.employee).subscribe(
      (_) => {
        this.employee = {
          id: 0,
          fullName: '',
          email: '',
          accessBlocks: '',
          password: '',
          activate: 'true',
          role: 'employee',
        };
        this.emp = {
          id: 0,
          fullName: '',
          email: '',
          accessBlocks: '',
          password: '',
          activate: 'true',
          role: 'employee',
        };
        this.accessBlocks = [];
      },
      (err) => {
        alert('Something went wrong');
        this.storageService.employeeInfo = [...employeeArray];
      }
    );
  }

  onCheckBox() {
    if (this.employee.activate == '') this.employee.activate = 'true';
    else if (this.employee.activate == 'true') this.employee.activate = 'false';
    else if (this.employee.activate == 'false') this.employee.activate = 'true';
  }
}
