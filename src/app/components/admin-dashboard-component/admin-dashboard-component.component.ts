import { Component, OnInit } from '@angular/core';
import { StorageServiceService } from 'src/app/services/storage-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState,
} from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { APIService } from 'src/app/services/api.service';
import {
  EmployeeInfo,
  DistributerInfo,
  ProductInfo,
  AccessBlocksInfo,
} from 'src/app/Models/userInfo';

@Component({
  selector: 'app-admin-dashboard-component',
  templateUrl: './admin-dashboard-component.component.html',
  styleUrls: ['./admin-dashboard-component.component.scss'],
})
export class AdminDashboardComponentComponent implements OnInit {
  selectedMenuItem: string = 'employee';
  isHandset: Observable<BreakpointState>;
  userName: string = '';

  constructor(
    private router: Router,
    private breakpointObserver: BreakpointObserver,
    public storageService: StorageServiceService,
    private apiService: APIService,
    private activatedRoute: ActivatedRoute

  ) {}

  ngOnInit(): void {
    this.isHandset = this.breakpointObserver.observe(Breakpoints.Handset);
    this.userName = this.activatedRoute.snapshot.params['userName'];
    this.getEmployeeList();
    this.getDistributerList();
    this.getProductList();
    this.getAccessBlockList();
  }

  getEmployeeList() {
    this.apiService.getEmployeeList().subscribe((data: EmployeeInfo[]) => {
      this.storageService.employeeInfo = data;
    });
  }
  getDistributerList() {
    this.apiService
      .getDistributerList()
      .subscribe((data: DistributerInfo[]) => {
        this.storageService.distributerInfo = data;
      });
  }
  getProductList() {
    this.apiService.getProductList().subscribe((data: ProductInfo[]) => {
      this.storageService.productInfo = data;
    });
  }
  getAccessBlockList() {
    this.apiService
      .getAccessBlockList()
      .subscribe((data: AccessBlocksInfo[]) => {
        this.storageService.accessBlocks = data;
      });
  }

  onManuClick(event) {
    this.selectedMenuItem = event.target.value;
  }

  onLogoutClick() {
    if (confirm('Do you want to sign out...?')) {
      this.storageService.loginDetails = null;
      this.router.navigate(['/login']);
    }
  }
}
