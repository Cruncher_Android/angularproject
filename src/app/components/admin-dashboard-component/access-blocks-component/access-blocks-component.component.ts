import { Component, OnInit } from '@angular/core';
import { AccessBlocksInfo } from './../../../Models/userInfo';
import { APIService } from 'src/app/services/api.service';
import { StorageServiceService } from 'src/app/services/storage-service.service';

@Component({
  selector: 'app-access-blocks-component',
  templateUrl: './access-blocks-component.component.html',
  styleUrls: ['./access-blocks-component.component.scss'],
})
export class AccessBlocksComponentComponent implements OnInit {
  accessBlock: AccessBlocksInfo;
  access: AccessBlocksInfo;
  constructor(
    private apiService: APIService,
    public storageService: StorageServiceService
  ) {}

  ngOnInit(): void {
    this.accessBlock = {
      id: 0,
      accessBlock: '',
    };
  }

  onEditAccessBlock(accessBlock) {
    this.accessBlock = { ...accessBlock };
    this.access = accessBlock;
  }

  onSaveAccessBlock() {
    let accessBlockExists: boolean = false;
    let accessBlockArray = [...this.storageService.accessBlocks];
    accessBlockArray.map((accessBlock) => {
      if (accessBlock.accessBlock == this.accessBlock.accessBlock) {
        alert('Distributer already exists');
        accessBlockExists = true;
        this.accessBlock = {
          id: 0,
          accessBlock: '',
        };
        this.access = {
          id: 0,
          accessBlock: '',
        };
      }
    });
    if (!accessBlockExists) {
      let index = this.storageService.accessBlocks.indexOf(this.access);
      if (index < 0) {
        if (this.storageService.accessBlocks.length > 0) {
          let id =
            this.storageService.accessBlocks[
              this.storageService.accessBlocks.length - 1
            ].id + 1;
          this.accessBlock.id = id;
        } else {
          this.accessBlock.id = 1;
        }
        this.storageService.accessBlocks.push(this.accessBlock);
      } else {
        this.storageService.accessBlocks[index] = this.accessBlock;
      }
      this.apiService.saveAccessBlockDetails(this.accessBlock).subscribe(
        (_) => {
          this.accessBlock = {
            id: 0,
            accessBlock: '',
          };
          this.access = {
            id: 0,
            accessBlock: '',
          };
        },
        (err) => {
          alert('Something went wrong');
          this.storageService.accessBlocks = [...accessBlockArray];
        }
      );
    }
  }
}
