import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessBlocksComponentComponent } from './access-blocks-component.component';

describe('AccessBlocksComponentComponent', () => {
  let component: AccessBlocksComponentComponent;
  let fixture: ComponentFixture<AccessBlocksComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccessBlocksComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccessBlocksComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
