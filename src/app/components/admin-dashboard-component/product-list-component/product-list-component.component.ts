import { Component, OnInit } from '@angular/core';
import { APIService } from './../../../services/api.service';
import { StorageServiceService } from './../../../services/storage-service.service';
import { ProductInfo } from 'src/app/Models/userInfo';

@Component({
  selector: 'app-product-list-component',
  templateUrl: './product-list-component.component.html',
  styleUrls: ['./product-list-component.component.scss'],
})
export class ProductListComponentComponent implements OnInit {
  product: ProductInfo;
  prod: ProductInfo;

  constructor(
    private apiService: APIService,
    public storageService: StorageServiceService
  ) {}

  ngOnInit(): void {
    this.product = {
      id: 0,
      productName: '',
    };
  }

  onEditProduct(product) {
    this.product = { ...product };
    this.prod = product;
  }

  onSaveProduct() {
    let productExists: boolean = false;
    let productArray = [...this.storageService.productInfo];
    productArray.map((product) => {
      if (product.productName == this.product.productName) {
        alert('Product already exists');
        productExists = true;
        this.product = {
          id: 0,
          productName: '',
        };
        this.prod = {
          id: 0,
          productName: '',
        };
        return;
      }
    });
    if (!productExists) {
      let index = this.storageService.productInfo.indexOf(this.prod);
      if (index < 0) {
        if (this.storageService.productInfo.length > 0) {
          let id =
            this.storageService.productInfo[
              this.storageService.productInfo.length - 1
            ].id + 1;
          this.product.id = id;
        } else {
          this.product.id = 1;
        }
        this.storageService.productInfo.push(this.product);
      } else {
        this.storageService.productInfo[index] = this.product;
      }
      this.apiService.saveProductDetails(this.product).subscribe(
        (_) => {
          this.product = {
            id: 0,
            productName: '',
          };
          this.prod = {
            id: 0,
            productName: '',
          };
        },
        (err) => {
          alert('Something went wrong');
          this.storageService.productInfo = [...productArray];
        }
      );
    }
  }
}
