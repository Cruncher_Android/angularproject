import { Component, OnInit } from '@angular/core';
import { DistributerInfo } from './../../../Models/userInfo';
import { APIService } from 'src/app/services/api.service';
import { StorageServiceService } from 'src/app/services/storage-service.service';

@Component({
  selector: 'app-distributer-list-component',
  templateUrl: './distributer-list-component.component.html',
  styleUrls: ['./distributer-list-component.component.scss'],
})
export class DistributerListComponentComponent implements OnInit {
  distributer: DistributerInfo;
  dist: DistributerInfo;
  constructor(
    private apiService: APIService,
    public storageService: StorageServiceService
  ) {}

  ngOnInit(): void {
    this.distributer = {
      id: 0,
      distributer: '',
      activate: 'true',
      email: '',
      password: '',
      role: 'distributer',
    };
  }

  onEditDistributer(distributer) {
    this.distributer = { ...distributer };
    this.dist = distributer;
  }

  // onSaveDistributer() {
  //   let distributerExists: boolean = false;
  //   let distributerArray = [...this.storageService.distributerInfo];
  //   distributerArray.map((distributer) => {
  //     if (distributer.id == this.distributer.id) {
  //       alert('Distributer already exists');
  //       distributerExists = true;
  //       this.distributer = {
  //         id: 0,
  //         distributer: '',
  //         activate: 'true',
  //         email: '',
  //         password: '',
  //         role:'distributer'
  //       };
  //       this.dist = {
  //         id: 0,
  //         distributer: '',
  //         activate: 'true',
  //         email: '',
  //         password: '',
  //         role:'distributer'
  //       };
  //     }
  //   });
  //   if (!distributerExists) {
  //     let index = this.storageService.distributerInfo.indexOf(this.dist);
  //     if (index < 0) {
  //       if (this.storageService.distributerInfo.length > 0) {
  //         let id =
  //           this.storageService.distributerInfo[
  //             this.storageService.distributerInfo.length - 1
  //           ].id + 1;
  //         this.distributer.id = id;
  //       } else {
  //         this.distributer.id = 1;
  //       }
  //       this.storageService.distributerInfo.push(this.distributer);
  //     } else {
  //       this.storageService.distributerInfo[index] = this.distributer;
  //     }
  //     this.apiService.saveDistributerDetails(this.distributer).subscribe(
  //       (_) => {
  //         this.distributer = {
  //           id: 0,
  //           distributer: '',
  //           activate: 'true',
  //           email: '',
  //           password: '',
  //           role:'distributer'
  //         };
  //         this.dist = {
  //           id: 0,
  //           distributer: '',
  //           activate: 'true',
  //           email: '',
  //           password: '',
  //           role:'distributer'
  //         };
  //       },
  //       (err) => {
  //         alert('Something went wrong');
  //         this.storageService.distributerInfo = [...distributerArray];
  //       }
  //     );
  //   }
  // }

  onSaveDistributer() {
    let distributerExists: boolean = false;
    let distributerArray = [...this.storageService.distributerInfo];
    let index = this.storageService.distributerInfo.indexOf(this.dist);
    if (index < 0) {
      distributerArray.map((distributer) => {
        if (
          distributer.distributer.toLowerCase() ==
          this.distributer.distributer.toLowerCase()
        ) {
          distributerExists = true;
          alert('distributer already exists');
          this.distributer = {
            id: 0,
            distributer: '',
            email: '',
            password: '',
            activate: 'true',
            role: 'distributer',
          };
          this.dist = {
            id: 0,
            distributer: '',
            email: '',
            password: '',
            activate: 'true',
            role: 'distributer',
          };
        }
      });
      if (!distributerExists) {
        if (this.storageService.distributerInfo.length > 0) {
          let id =
            this.storageService.distributerInfo[
              this.storageService.distributerInfo.length - 1
            ].id + 1;
          this.distributer.id = id;
        } else {
          this.distributer.id = 1;
        }
        this.storageService.distributerInfo.push(this.distributer);
        this.save(distributerArray);
      }
    } else {
      this.storageService.distributerInfo[index] = this.distributer;
      this.save(distributerArray);
    }
  }

  save(distributerArray) {
    this.apiService.saveDistributerDetails(this.distributer).subscribe(
      (_) => {
        this.distributer = {
          id: 0,
          distributer: '',
          email: '',
          password: '',
          activate: 'true',
          role: 'distributer',
        };
        this.dist = {
          id: 0,
          distributer: '',
          email: '',
          password: '',
          activate: 'true',
          role: 'distributer',
        };
      },
      (err) => {
        alert('Something went wrong');
        this.storageService.distributerInfo = [...distributerArray];
      }
    );
  }

  onCheckBox() {
    if (this.distributer.activate == '') this.distributer.activate = 'true';
    else if (this.distributer.activate == 'true')
      this.distributer.activate = 'false';
    else if (this.distributer.activate == 'false')
      this.distributer.activate = 'true';
  }
}
