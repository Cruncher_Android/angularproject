import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributerListComponentComponent } from './distributer-list-component.component';

describe('DistributerListComponentComponent', () => {
  let component: DistributerListComponentComponent;
  let fixture: ComponentFixture<DistributerListComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistributerListComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributerListComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
