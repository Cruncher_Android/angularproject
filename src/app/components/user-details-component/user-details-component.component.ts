import { Component, OnInit } from '@angular/core';
import { StorageServiceService } from './../../services/storage-service.service';
import { UserInfo } from './../../Models/userInfo';
import { Router } from '@angular/router';
import { APIService } from './../../services/api.service';

@Component({
  selector: 'app-user-details-component',
  templateUrl: './user-details-component.component.html',
  styleUrls: ['./user-details-component.component.scss'],
})
export class UserDetailsComponentComponent implements OnInit {
  userInfo: UserInfo;
  distributerName: string = '';
  appName: string = '';
  constructor(
    private router: Router,
    private storageService: StorageServiceService,
    private apiService: APIService
  ) {}

  ngOnInit(): void {
    this.userInfo = this.storageService.userInfo;
    this.distributerName = this.storageService.distributerInfo[Number.parseInt(this.userInfo.distributerId) - 1].distributer
    this.appName = this.storageService.productInfo[Number.parseInt(this.userInfo.appId) - 2].productName
  }

  onDeactivateClick() {
    this.apiService.deactivateAccount(this.userInfo).subscribe((data) => {
      if (data) this.userInfo.activated = 'false';
    });
  }

  onActivateClick() {
    this.apiService.activateAccount(this.userInfo).subscribe((data) => {
      if (data) this.userInfo.activated = 'true';
    });
  }
}
