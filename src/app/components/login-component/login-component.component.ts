import { Component, OnInit } from '@angular/core';
import { loginDetails } from 'src/app/Models/userInfo';
import { APIService } from './../../services/api.service';
import { StorageServiceService } from './../../services/storage-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.scss'],
})
export class LoginComponentComponent implements OnInit {
  loginDetails: loginDetails;
  showSpinner: boolean = false;

  constructor(
    private router: Router,
    private apiService: APIService,
    private storageService: StorageServiceService
  ) {}

  ngOnInit(): void {
    this.loginDetails = {
      id: 0,
      fullName: '',
      email: '',
      password: '',
      activate: '',
      role: '',
      distributer: '',
    };
    // let regiDateFormated: any[] = [];
    // this.apiService.getRegiDates().subscribe((data: any[]) => {
    //   console.log(data);
    //   data.map((d) => {
    //     if (d.date != null) {
    //       regiDateFormated.push({
    //         date: d.date,
    //         dateFormated: `${d.date.split('-')[2]}-${
    //           d.date.split('-')[1]
    //         }-${d.date.split('-')[0]}`,
    //       });
    //     }
    //   });
    //   console.log(regiDateFormated);

    //     this.apiService
    //       .setRegiDate(regiDateFormated)
    //       .subscribe((data) => console.log(data));
    // });
  }

  onLoginClick() {
    this.showSpinner = true;
    this.apiService.login(this.loginDetails).subscribe(
      (data: loginDetails) => {
        this.showSpinner = false;
        // this.storageService.loginDetails = data;
        if (data.role == 'admin')
          this.router.navigate(['/admin-dashboard', data.fullName]);
        else if (data.role == 'employee') {
          if (data.activate == 'true')
            this.router.navigate(['/employee-dashboard', data.fullName]);
          else if (data.activate == 'false')
            alert('Restricted from login, contact to admin');
        } else if (data.role == 'distributer') {
          if (data.activate == 'true')
            this.router.navigate([
              '/distributer-dashboard',
              data.distributer,
              data.id,
            ]);
          else if (data.activate == 'false')
            alert('Restricted from login, contact to admin');
        }
        console.log('data', data);
      },
      (err) => {
        this.showSpinner = false;
        alert('Invalid login details');
      }
    );
  }
}
