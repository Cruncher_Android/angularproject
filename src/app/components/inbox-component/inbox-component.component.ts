import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { APIService } from './../../services/api.service';
import { UserInfo } from './../../Models/userInfo';
import { StorageServiceService } from './../../services/storage-service.service';
@Component({
  selector: 'app-inbox-component',
  templateUrl: './inbox-component.component.html',
  styleUrls: ['./inbox-component.component.scss'],
})
export class InboxComponentComponent implements OnInit {
  userInfo: UserInfo[] = [];
  constructor(private router: Router, private apiService: APIService, public storageServiceService: StorageServiceService) {}

  ngOnInit(): void {
  }

  onGetDetails(userInfo) {
    this.storageServiceService.userInfo = userInfo;
    this.router.navigate(['registeration'])
  }
}
