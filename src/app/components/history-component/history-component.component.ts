import { Component, OnInit, Input } from '@angular/core';
import { APIService } from '../../services/api.service';
import { UserInfo } from '../../Models/userInfo';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { MatDialog } from '@angular/material/dialog';
import { StorageServiceService } from '../../services/storage-service.service';
import { UserDetailsComponentComponent } from '../user-details-component/user-details-component.component';
import { ExcelServiceService } from './../../services/excel-service.service';

@Component({
  selector: 'app-history-component',
  templateUrl: './history-component.component.html',
  styleUrls: ['./history-component.component.scss'],
})
export class HistoryComponentComponent implements OnInit {
  @Input() isDistributerLogin: boolean;
  @Input() distributerId: string = '';
  userInfo: UserInfo[] = [];
  filteredUsers: UserInfo[] = [];
  activeMonth = '';
  activeDist = '';
  distributerName: string = '';
  monthName: string = '';
  query: string = '';
  dist: string = '';
  showAdvancedSearchPanel: boolean = false;
  fromDate: string = '';
  toDate: string = '';
  fromDateString: string = '';
  toDateString: string = '';
  datepickerConfig: Partial<BsDatepickerConfig>;

  constructor(
    private dialog: MatDialog,
    private apiService: APIService,
    public storageService: StorageServiceService,
    private excelService: ExcelServiceService
  ) {
    this.datepickerConfig = {
      containerClass: 'theme-dark-blue',
      showWeekNumbers: false,
      dateInputFormat: 'DD/MM/YYYY',
      isAnimated: true,
      useUtc: false,
    };
  }

  ngOnInit(): void {
    console.log('in ng on init');
    this.userInfo = [...this.storageService.allUsers];
    this.filteredUsers = [...this.storageService.allUsers];
  }

  onUserClick(user) {
    this.storageService.userInfo = user;
    this.dialog.open(UserDetailsComponentComponent);
  }

  onClearAllFilters() {
    this.activeMonth = '';
    this.activeDist = '';
    this.dist = '';
    this.distributerName = '';
    this.monthName = '';
    this.userInfo = [...this.storageService.allUsers];
    this.filteredUsers = [...this.storageService.allUsers];
  }

  filterByDistributer(id, name) {
    this.dist = id.toString();
    if (this.dist.length == 1) this.activeDist = '0' + this.dist;
    else this.activeDist = this.dist;
    this.distributerName = name;
  }

  filterByMonth(id, name) {
    this.activeMonth = id;
    this.monthName = name;
  }

  onClearMonthFilter() {
    this.activeMonth = '';
    this.monthName = '';
  }

  onClearDistFilter() {
    this.activeDist = '';
    this.dist = '';
    this.distributerName = '';
  }

  applyFilter() {
    this.userInfo = [...this.storageService.allUsers];
    if (this.activeMonth !== '') {
      this.userInfo = this.userInfo.filter(
        (user) => user.regiDate.split('-')[1] === this.activeMonth
      );
    }
    if (this.activeDist !== '') {
      this.userInfo = this.userInfo.filter(
        (user) => user.distributerId === this.activeDist
      );
    }
    this.filteredUsers = [...this.userInfo];
  }

  handleSearch(event) {
    let query: string = event.target.value;
    let userInfo: UserInfo[] = [];
    userInfo = this.filteredUsers.filter((user) =>
      user.firstName.toLowerCase().startsWith(query.toLowerCase())
    );
    this.userInfo = [...userInfo];
  }

  onAdvancedSearch() {
    this.showAdvancedSearchPanel = true;
  }

  onClose() {
    this.showAdvancedSearchPanel = false;
    this.fromDate = '';
    this.toDate = '';
    this.userInfo = this.storageService.allUsers;
  }

  onSearchButton() {
    this.toDateString = this.getFormatedDate(this.toDate.toString().split(' '));
    this.fromDateString = this.getFormatedDate(
      this.fromDate.toString().split(' ')
    );

    // console.log(
    //   'toDateString',
    //   this.toDateString,
    //   'fromDateString',
    //   this.fromDateString
    // );

    this.apiService
      .getBetweenTimeStamp(
        {
          fromDate: this.fromDateString,
          toDate: this.toDateString,
        },
        this.distributerId
      )
      .subscribe((data: UserInfo[]) => {
        this.userInfo = data;
      });
  }

  exportToExcel() {
    let userInfo: {}[] = [];
    this.userInfo.map((user) => {
      // console.log(user)
      let date1 = new Date(
        `${user.regiDate.split('-')[1]}/${user.regiDate.split('-')[0]}/${
          user.regiDate.split('-')[2]
        }`
      );
      let date2 = new Date(
        `${user.expireDate.split('-')[1]}/${user.expireDate.split('-')[0]}/${
          user.expireDate.split('-')[2]
        }`
      );
      let Difference_In_Time = date2.getTime() - date1.getTime();
      let Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
      userInfo.push({
        FirstName: user.firstName,
        LastName: user.lastName,
        Contact: user.contactNo,
        EmailId: user.email,
        ActivationDate: user.regiDate,
        ExpiryDate: user.expireDate,
        NumberOfDays: Difference_In_Days,
        ProductName:
          user.appId == '2'
            ? 'JEE'
            : user.appId == '3'
            ? 'NEET'
            : user.appId == '4'
            ? 'CET-PCM'
            : user.appId == '5'
            ? 'CET-PCB'
            : '',
        ActivationGivenBY: user.activationGivenBy,
      });
    });
    this.excelService.exportAsExcelFile(userInfo, 'registered_data');
  }

  getFormatedDate(dateArray) {
    let date = dateArray[2];
    if (Number.parseInt(date) < 10) date = date.substr(1);
    let month = dateArray[1];
    if (month == 'Jan') month = '1';
    if (month == 'Feb') month = '2';
    if (month == 'Mar') month = '3';
    if (month == 'Apr') month = '4';
    if (month == 'May') month = '5';
    if (month == 'Jun') month = '6';
    if (month == 'Jul') month = '7';
    if (month == 'Aug') month = '8';
    if (month == 'Sep') month = '9';
    if (month == 'Oct') month = '10';
    if (month == 'Nov') month = '11';
    if (month == 'Dec') month = '12';
    let year = dateArray[3];
    return `${year}-${month}-${date}`;
  }
}
