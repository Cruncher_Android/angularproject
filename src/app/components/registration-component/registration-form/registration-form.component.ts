import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserInfo, TimeStamp } from 'src/app/Models/userInfo';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss'],
})
export class RegistrationFormComponent implements OnInit {
  @Input() userInfo: UserInfo;
  @Output() onActivate = new EventEmitter<any>();
  timeStamp: TimeStamp;
  verificationKey: string = '';
  expireOn: string = '';
  registerOn: string ='';
  distributer: string = '';
  application: string = '';

  constructor(private http: HttpClient) {}

  ngOnInit(): void {

    let registerDate = this.userInfo.regiDate.substr(0,2);
    let registerMonth = this.userInfo.regiDate.substr(3,2);
    if(registerMonth == "01") registerMonth = "Jan"
    else if(registerMonth == "02") registerMonth = "Feb"
    else if(registerMonth == "03") registerMonth = "Mar"
    else if(registerMonth == "04") registerMonth = "Apr"
    else if(registerMonth == "05") registerMonth = "May"
    else if(registerMonth == "06") registerMonth = "Jun"
    else if(registerMonth == "07") registerMonth = "July"
    else if(registerMonth == "08") registerMonth = "Aug"
    else if(registerMonth == "09") registerMonth = "Sep"
    else if(registerMonth == "10") registerMonth = "Oct"
    else if(registerMonth == "11") registerMonth = "Nov"
    else if(registerMonth == "12") registerMonth = "Dec"
    let registerYear = this.userInfo.regiDate.substr(6);
    this.registerOn = registerDate + "-" + registerMonth + "-" + registerYear;

    this.timeStamp = {
      deviceId: this.userInfo.deviceId,
      appId: this.userInfo.appId,
      distributerId: this.userInfo.distributerId,
      yearsCount: 0,
      monthsCount: 0,
      daysCount: 0,
      regiDate: 0,
      regiMonth: 0,
      regiYear: 0,
      date: ''
    };
  }

  onGenerateKey() {
    this.http
      .post('http://localhost:8080/userRegister', this.timeStamp)
      .subscribe((data) => {
        console.log(data);
        this.verificationKey = data[0];
        let expiryDay =
          this.verificationKey.substr(10, 1) +
          this.verificationKey.substr(12, 1)
        let expiryMonth = this.verificationKey.substr(0, 1);
        if (expiryMonth == 'A') expiryMonth = 'Jan';
        else if (expiryMonth == 'B') expiryMonth = 'Feb';
        else if (expiryMonth == 'C') expiryMonth = 'Mar';
        else if (expiryMonth == 'D') expiryMonth = 'Apr';
        else if (expiryMonth == 'E') expiryMonth = 'May';
        else if (expiryMonth == 'F') expiryMonth = 'Jun';
        else if (expiryMonth == 'G') expiryMonth = 'July';
        else if (expiryMonth == 'H') expiryMonth = 'Aug';
        else if (expiryMonth == 'I') expiryMonth = 'Sep';
        else if (expiryMonth == 'J') expiryMonth = 'Oct';
        else if (expiryMonth == 'K') expiryMonth = 'Nov';
        else if (expiryMonth == 'L') expiryMonth = 'Dec';
        let expiryYear =
          this.verificationKey.substr(5, 1) +
          this.verificationKey.substr(7, 1);
        this.expireOn = expiryDay + "-" + expiryMonth + "-" + "20" + expiryYear;
      });
  }

  onActivateClick() {
    this.onActivate.emit();
  }
}
