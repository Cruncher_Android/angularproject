import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { UserInfo, TimeStamp } from '../../Models/userInfo';
import { APIService } from './../../services/api.service';
import { StorageServiceService } from './../../services/storage-service.service';

@Component({
  selector: 'app-registration-component',
  templateUrl: './registration-component.component.html',
  styleUrls: ['./registration-component.component.scss'],
})
export class RegistrationComponentComponent implements OnInit {
  userInfo: UserInfo;
  userName: string = '';
  timeStamp: TimeStamp;
  verificationKey: string = '';
  expireOn: string = '';
  expireDate: string = '';
  registerOn: string = '';
  distributer: string = '';
  application: string = '';
  deviceId: string = '';
  index: number = 0;
  radioValue: string = '';
  deviceIdMatched: boolean = true;
  reRegisteration: boolean = false;
  date: any;
  month: any;
  year: any;
  fullYear: any;
  enteredDaysCount: number;
  showSpinner: boolean = false;
  distributerId: number = 0;
  appId: number = 0;
  datepickerConfig: Partial<BsDatepickerConfig>;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private apiService: APIService,
    public storageServiceService: StorageServiceService
  ) {
    this.date = new Date().getDate();
    this.month = new Date().getMonth() + 1;
    this.fullYear = new Date().getFullYear();
    this.year = parseInt(new Date().getFullYear().toString().substr(2));
    this.datepickerConfig = {
      containerClass: 'theme-dark-blue',
      showWeekNumbers: false,
      dateInputFormat: 'DD/MM/YYYY',
      isAnimated: true,
      useUtc: false,
      minDate: new Date(this.fullYear, this.month - 1, this.date),
      maxDate: new Date(this.fullYear + 2, this.month - 1, this.date),
    };
  }

  ngOnInit(): void {
    this.userInfo = {
      DOB: '',
      appId: '',
      appKey: '',
      contactNo: '',
      deviceId: '',
      distributerId: '',
      district: '',
      email: '',
      expireDate: '',
      firstName: '',
      gender: '',
      lastName: '',
      parentsContactNo: '',
      password: '',
      regiDate: '',
      standard: '',
      state: '',
      status: '',
      taluka: '',
      activated: '',
      activationGivenBy: '',
      notiDate: '',
      notiTime: '',
    };
  }

  onGenerateKey() {
    console.log(this.userInfo.distributerId);
    if (this.radioValue != '') {
      this.userInfo.regiDate =
        this.date.toString() +
        '-' +
        this.month.toString() +
        '-' +
        this.fullYear.toString();

      if (this.radioValue == '6Months') {
        this.timeStamp = {
          deviceId: this.userInfo.deviceId,
          appId: this.userInfo.appId,
          distributerId: this.userInfo.distributerId,
          daysCount: 0,
          monthsCount: 6,
          yearsCount: 0,
          regiDate: this.date,
          regiMonth: this.month,
          regiYear: this.year,
          date: '',
        };
      } else if (this.radioValue == '1Year') {
        this.timeStamp = {
          deviceId: this.userInfo.deviceId,
          appId: this.userInfo.appId,
          distributerId: this.userInfo.distributerId,
          daysCount: 0,
          monthsCount: 0,
          yearsCount: 1,
          regiDate: this.date,
          regiMonth: this.month,
          regiYear: this.year,
          date: '',
        };
      } else if (this.radioValue == '2Years') {
        this.timeStamp = {
          deviceId: this.userInfo.deviceId,
          appId: this.userInfo.appId,
          distributerId: this.userInfo.distributerId,
          daysCount: 0,
          monthsCount: 0,
          yearsCount: 2,
          regiDate: this.date,
          regiMonth: this.month,
          regiYear: this.year,
          date: '',
        };
      } else if (this.radioValue == 'date') {
        this.timeStamp = {
          deviceId: this.userInfo.deviceId,
          appId: this.userInfo.appId,
          distributerId: this.userInfo.distributerId,
          daysCount: 0,
          monthsCount: 0,
          yearsCount: 0,
          regiDate: this.date,
          regiMonth: this.month,
          regiYear: this.year,
          date: JSON.stringify(this.expireDate).substr(1, 10),
        };
        // console.log(this.timeStamp.date);
      } else if (this.radioValue == 'days') {
        // console.log(typeof this.enteredDaysCount);
        let daysCount = this.enteredDaysCount % 30;
        let monthsCount = Math.floor(this.enteredDaysCount / 30);
        let date = this.date + daysCount;
        let month = this.month + monthsCount;
        let year = this.fullYear;
        if (date > 30) {
          date -= 30;
          month += 1;
        }
        if (month > 12) {
          month -= 12;
          year += 1;
        }
        let dateStr =
          date.toString().length == 1 ? '0' + date.toString() : date.toString();
        let monthStr =
          month.toString().length == 1
            ? '0' + month.toString()
            : month.toString();
        let yearStr = year.toString();
        // console.log(dateStr, monthStr, yearStr)
        // console.log(typeof(date), typeof(month), typeof(year));
        this.timeStamp = {
          deviceId: this.userInfo.deviceId,
          appId: this.userInfo.appId,
          distributerId: this.userInfo.distributerId,
          daysCount: 0,
          monthsCount: 0,
          yearsCount: 0,
          regiDate: this.date,
          regiMonth: this.month,
          regiYear: this.year,
          date: yearStr + '-' + monthStr + '-' + dateStr,
        };
        // console.log(this.timeStamp.date)
      }
      this.apiService.registerUser(this.timeStamp).subscribe((data) => {
        this.userInfo.appKey = data[0];
        this.userInfo.expireDate = data[1];
      });
    } else {
      alert('Set Expiry date to generate key');
    }
  }

  onActivateClick() {
    this.showSpinner = true;
    // console.log(this.userInfo);
    // this.storageServiceService.allUsers.map(user => {
    //   if(user.email == this.userInfo.email) {
    //     this.showSpinner = false;
    //     this.userInfo = user;
    //     alert('User already exists');
    //     return;
    //   }
    // })
    console.log('after return');
    this.userName = this.activatedRoute.snapshot.params['userName'];
    this.userInfo.activationGivenBy = this.userName;
    this.userInfo.status = 'loggedIn';
    this.userInfo.activated = 'true';
    this.apiService.setUser(this.userInfo).subscribe(
      (data) => {
        this.showSpinner = false;
        alert('Activate Successfully');
        this.storageServiceService.allUsers.unshift(this.userInfo);
        // this.apiService.getPendingRequests();
        // this.apiService.getAllUsers();
      },
      (err) => alert('Activation fail')
    );
  }

  // onDeactivateClick() {
  //   this.showSpinner = true;
  //   this.apiService.deactivateAccount(this.userInfo.email).subscribe(
  //     (data) => {
  //       if (data) {
  //         this.showSpinner = false;
  //         alert('Deactivate Successfully');
  //         // this.apiService.getPendingRequests();
  //       }
  //     },
  //     (err) => alert('Deactivation fail')
  //   );
  // }

  onClearClick() {
    this.userInfo = {
      DOB: '',
      appId: '',
      appKey: '',
      contactNo: '',
      deviceId: '',
      distributerId: '',
      district: '',
      email: '',
      expireDate: '',
      firstName: '',
      gender: '',
      lastName: '',
      parentsContactNo: '',
      password: '',
      regiDate: '',
      standard: '',
      state: '',
      status: '',
      taluka: '',
      activated: '',
      activationGivenBy: '',
      notiDate: '',
      notiTime: '',
    };
    this.deviceId == '';
    this.distributerId = 0;
  }

  onRadioChange(ev) {
    // console.log('in radio');
    this.radioValue = ev.target.value;
  }

  onDeviceIdSearch() {
    this.apiService.getUser(this.userInfo.deviceId).subscribe(
      (data: UserInfo) => {
        this.reRegisteration = true;
        this.userInfo = data;
        // console.log(this.userInfo);
      },
      (err) => {
        this.reRegisteration = false;
        alert('No record found');
      }
    );
  }

  onEmailSearch() {
    this.deviceId = this.userInfo.deviceId;
    this.apiService.searchByEmail(this.userInfo.email).subscribe(
      (data: UserInfo) => {
        this.reRegisteration = true;
        this.userInfo = data;
        if (this.deviceId != this.userInfo.deviceId)
          this.deviceIdMatched = false;
        else this.deviceIdMatched = true;
      },
      (err) => {
        this.reRegisteration = false;
        alert('No record found');
      }
    );
  }

  onGetDetails(userInfo: UserInfo) {
    // if (
    //   this.storageServiceService.allUsers.find(
    //     (user) => user.email === userInfo.email
    //   ) == undefined
    // )
    //   this.userInfo = { ...userInfo };

    // else alert('Record already exists');
    this.userInfo = { ...userInfo };
    console.log(userInfo)
    this.distributerId =
      Number.parseInt(userInfo.distributerId) < 10
        ? Number.parseInt(userInfo.distributerId.substr(1))
        : Number.parseInt(userInfo.distributerId);
    this.appId = Number.parseInt(userInfo.appId);
  }

  onDistributer(event) {
    console.log(event.target.value);
    let distributerId = event.target.value;
    distributerId.length == 1
      ? (this.userInfo.distributerId = '0' + distributerId)
      : (this.userInfo.distributerId = distributerId);
  }

  onApplication(event) {
    console.log(event.target.value);
    this.userInfo.appId = event.target.value;
  }
}
