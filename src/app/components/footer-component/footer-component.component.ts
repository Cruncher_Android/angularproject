import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer-component',
  templateUrl: './footer-component.component.html',
  styleUrls: ['./footer-component.component.scss']
})
export class FooterComponentComponent implements OnInit {

currentYear: string ="";

  constructor() { }

  ngOnInit(): void {
    this.currentYear = new Date().getFullYear().toString();
  }

}
