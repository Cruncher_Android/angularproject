import { Component, OnInit } from '@angular/core';
import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState,
} from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { StorageServiceService } from 'src/app/services/storage-service.service';
import {
  UserInfo,
  DistributerInfo,
  ProductInfo,
  demoInfo
} from 'src/app/Models/userInfo';
import { APIService } from 'src/app/services/api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-employee-dashboard-component',
  templateUrl: './employee-dashboard-component.component.html',
  styleUrls: ['./employee-dashboard-component.component.scss'],
})
export class EmployeeDashboardComponentComponent implements OnInit {
  isHandset: Observable<BreakpointState>;
  timeout: any;
  selectedMenuItem: string = 'registeration';
  selectedNavBarItem: string = 'requests';
  userName: string = '';
  isDistributerLogin: boolean = false;

  constructor(
    private router: Router,
    private breakpointObserver: BreakpointObserver,
    public storageService: StorageServiceService,
    private apiService: APIService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.isHandset = this.breakpointObserver.observe(Breakpoints.Handset);
    this.userName = this.activatedRoute.snapshot.params['userName'];
    this.getPendingNotifications();
    this.getAllUsers();
    this.getDistributerList();
    this.getProductList();
    this.getDemoList();
    this.timeout = setInterval(() => {
      this.getPendingNotifications();
    }, 10000);
  }

  getDistributerList() {
    this.apiService
      .getDistributerList()
      .subscribe((data: DistributerInfo[]) => {
        this.storageService.distributerInfo = data;
      });
  }

  getProductList() {
    this.apiService.getProductList().subscribe((data: ProductInfo[]) => {
      this.storageService.productInfo = data;
    });
  }

  getAllUsers() {
    this.apiService.getAllUsers().subscribe((data: UserInfo[]) => {
      // console.log("in all users")
      this.storageService.allUsers = data;
    });
  }

  getDemoList() {
    this.apiService.getDemoDetails().subscribe((data: demoInfo[]) => {
      this.storageService.demoUsers = data;
    } )
  }

  getPendingNotifications() {
    this.apiService.getPendingRequests().subscribe((data: UserInfo[]) => {
      this.storageService.userInfoArray = data;
    });
  }
  onManuClick(event) {
    this.selectedMenuItem = event.target.value;
  }

  onNavBarClick(event) {
    this.selectedNavBarItem = event.target.value;
  }

  onLogoutClick() {
    if (confirm('Do you want to sign out...?')) {
      clearTimeout(this.timeout);
      this.storageService.loginDetails = null;
      this.router.navigate(['/login']);
      this.storageService.allUsers = [];
      this.storageService.demoUsers = [];
    }
  }
}
