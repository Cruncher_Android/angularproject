import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterationComponentComponent } from './registeration-component.component';

describe('RegisterationComponentComponent', () => {
  let component: RegisterationComponentComponent;
  let fixture: ComponentFixture<RegisterationComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterationComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterationComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
