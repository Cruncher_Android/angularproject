import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TimeStamp } from 'src/app/Models/userInfo';
import { UserInfo } from './../Models/userInfo';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class APIService {
  url: string = 'https://cruncher-spring-boot.herokuapp.com/';
  // url: string = 'https://crunchersoft-test.herokuapp.com/';
  // url: string = 'http://localhost:8080/';
  constructor(private http: HttpClient) {}

  //Employee Side
  registerUser(timeStamp) {
    return this.http.post(`${this.url}userRegister`, timeStamp);
  }

  setUser(userInfo) {
    return this.http.post(`${this.url}setUserInfo`, userInfo);
  }

  getUser(deviceId) {
    return this.http.post(`${this.url}getUserInfo`, { deviceId: deviceId });
  }

  getAllUsers() {
    return this.http.get(`${this.url}getAllUsers`);
  }

  getAllUsersDistributerWise(distributerId) {
    return this.http.get(
      `${this.url}getAllUsersDistributerWise/?distributerId=${distributerId}`
    );
  }

  searchByEmail(email) {
    return this.http.post(`${this.url}searchByEmail`, {
      email: email,
    });
  }

  getPendingRequests() {
    return this.http.get(`${this.url}getNotificationDetails`);
  }

  getDemo() {
    return this.http.get(`${this.url}getDemo`);
  }

  getDemoDetail() {
    return this.http.get(`${this.url}getDemoDetail`);
  }

  getDemoDetails() {
    return this.http.get(`${this.url}getDemoDetails`);
  }

  getDistributerWiseDemoDetails(distributerId) {
    return this.http.get(
      `${this.url}getDistributerWiseDemoDetails/?distributerId=${distributerId}`
    );
  }

  deactivateAccount(userInfo) {
    console.log(userInfo);
    return this.http.post(`${this.url}deactivateAccount`, userInfo);
  }

  activateAccount(userInfo) {
    return this.http.post(`${this.url}activateAccount`, userInfo);
  }

  //Admin Side

  getEmployeeList() {
    return this.http.get(`${this.url}getEmployee`);
  }

  getDistributerList() {
    return this.http.get(`${this.url}getDistributer`);
  }

  getProductList() {
    return this.http.get(`${this.url}getProduct`);
  }

  getAccessBlockList() {
    return this.http.get(`${this.url}getAccessBlock`);
  }

  saveEmployeeDetails(employee) {
    return this.http.post(`${this.url}addEmployee`, employee);
  }

  saveDistributerDetails(distributer) {
    return this.http.post(`${this.url}addDistributer`, distributer);
  }

  saveProductDetails(product) {
    return this.http.post(`${this.url}addProduct`, product);
  }

  saveAccessBlockDetails(accessBlock) {
    return this.http.post(`${this.url}addAccessBlock`, accessBlock);
  }

  login(loginDetails) {
    return this.http.post(`${this.url}home`, loginDetails);
  }

  getRegiDates() {
    return this.http.get(`${this.url}getRegiDates`)
  }

  setRegiDate(regiDateFormatedArray) {
    return this.http.post(`${this.url}setRegiDates`, regiDateFormatedArray)
  }

  getBetweenTimeStamp(timeStamp, distributerId) {
    return this.http.post(`${this.url}/getBetweenTimeStamp/?distributerId=${distributerId}`, timeStamp)
  }

  getBetweenTimeStampDemo(timeStamp, distributerId) {
    return this.http.post(`${this.url}/getBetweenTimeStampDemo/?distributerId=${distributerId}`, timeStamp)
  }
}
