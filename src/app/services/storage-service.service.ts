import { Injectable } from '@angular/core';
import {
  UserInfo,
  DistributerInfo,
  ProductInfo,
  AccessBlocksInfo,
  loginDetails,
} from './../Models/userInfo';
import { EmployeeInfo, demoInfo } from 'src/app/Models/userInfo';

@Injectable({
  providedIn: 'root',
})
export class StorageServiceService {
  userInfo: UserInfo;
  userInfoArray: UserInfo[] = [];
  allUsers: UserInfo[] = [];
  employeeInfo: EmployeeInfo[] = [];
  distributerInfo: DistributerInfo[] = [];
  productInfo: ProductInfo[] = [];
  accessBlocks: AccessBlocksInfo[] = [];
  demoUsers: demoInfo[] = [];
  loginDetails: loginDetails;
  constructor() {}
}
