//Employee side

export interface UserInfo {
  firstName: string;
  lastName: string;
  contactNo: string;
  email: string;
  password: string;
  DOB: string;
  gender: string;
  parentsContactNo: string;
  standard: string;
  state: string;
  district: string;
  taluka: string;
  distributerId: string;
  appId: string;
  deviceId: string;
  appKey: string;
  regiDate: string;
  expireDate: string;
  notiDate: string;
  notiTime: string;
  status: string;
  activated: string;
  activationGivenBy: string;
}

export interface TimeStamp {
  deviceId: string;
  appId: string;
  distributerId: string;
  daysCount: number;
  monthsCount: number;
  yearsCount: number;
  regiDate: number;
  regiMonth: number;
  regiYear: number;
  date: string;
}

export interface demoInfo {
  firstName: string;
  lastName: string;
  contactNo: string;
  email: string;
  distributerId: String;
  appId: String;
  date: string;
  time: string;
  area: string;
}

//Admin Side

export interface EmployeeInfo {
  id: number;
  fullName: string;
  email: string;
  accessBlocks: string;
  password: string;
  activate: string;
  role: string;
}

export interface DistributerInfo {
  id: number;
  distributer: string;
  email: string;
  password: string;
  activate: string;
  role: string;
}

export interface ProductInfo {
  id: number;
  productName: string;
}

export interface AccessBlocksInfo {
  id: number;
  accessBlock: string;
}

export interface loginDetails {
  id: number
  fullName: string;
  email: string;
  password: string;
  activate: string;
  role: string;
  distributer: string
}
