import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminDashboardComponentComponent } from './components/admin-dashboard-component/admin-dashboard-component.component';
import { EmployeeDashboardComponentComponent } from './components/employee-dashboard-component/employee-dashboard-component.component';
import { LoginComponentComponent } from './components/login-component/login-component.component';
import { DistributerDashboardComponentComponent } from './components/distributer-dashboard-component/distributer-dashboard-component.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponentComponent,
  },
  {
    path: 'admin-dashboard/:userName',
    component: AdminDashboardComponentComponent,
  },
  {
    path: 'employee-dashboard/:userName',
    component: EmployeeDashboardComponentComponent,
  },
  {
    path: 'distributer-dashboard/:distributerName/:distributerId',
    component: DistributerDashboardComponentComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule],
})
export class AppRoutingModule {}
